#ifndef SHADERS_H_
#define SHADERS_H_

#include <GL/glew.h>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

#include <stdbool.h>

#include "game_types.h"

int shader_load_from_file(shader_t *out,
                          const char *vertex_path, const char *fragment_path);
void shader_use(shader_t *shader);
void shader_stop();

int atlas_create_png(texture_atlas_t *out, const char *filepath);
void atlas_free(texture_atlas_t *atlas);
void texture_create(texture_t *out, texture_atlas_t *atlas,
                    uint32_t xmin, uint32_t ymin,
                    uint32_t xmax, uint32_t ymax);
int texture_create_sheet(texture_t **out, texture_atlas_t *atlas,
                         uint32_t xmin, uint32_t ymin,
                         uint32_t xmax, uint32_t ymax,
                         uint32_t w, uint32_t h);
void texture_destroy_sheet(texture_t **sheet);

void font_init();
int font_create(font_t out, texture_atlas_t *atlas, uint32_t char_w, uint32_t char_h);
void text_create(text_sprite_t *out, font_t *font, char *str);
void text_update(text_sprite_t *text, bool tile_pos);
void text_render_begin();
void text_render_end();
void text_render(text_sprite_t *text);
void text_destroy(text_sprite_t *text);

void sprite_init();
void sprite_create(sprite_t *out, texture_t *texture);
void sprite_update(sprite_t *sprite);
void sprite_render_begin();
void sprite_render_end();
void sprite_render(sprite_t *sprite);

#endif // SHADERS_H_
