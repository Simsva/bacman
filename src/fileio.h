#ifndef FILEIO_H_
#define FILEIO_H_

#include <stdio.h>

size_t file_get_size(FILE *fp);
size_t file_read_to_buffer(char **out, const char *filepath);

#endif // FILEIO_H_
