#include "shaders.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <setjmp.h>

#include <png.h>

#include "game_types.h"
#include "prefix_dirs.h"
#include "fileio.h"

/* shader */
int shader_load_from_file(shader_t *out,
                           const char *vertex_path, const char *fragment_path) {
    size_t fs;
    char *vertex_text, *fragment_text, filepath_buf[256];

    /* logging */
    GLint result;
    int log_size, log_size_max;
    char *log;

    /* compile vertex shader */
    snprintf(filepath_buf, sizeof(filepath_buf), DATADIR "/shaders/%s", vertex_path);
    fs = file_read_to_buffer(&vertex_text, filepath_buf);
    if(fs == -1) {
        // TODO: Logging
        fprintf(stderr, "[Shader ERR] %s\n", "No such vertex shader file");
        return -1;
    }
    out->vert = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(out->vert, 1, (const char *const *)&vertex_text, NULL);
    glCompileShader(out->vert);

    free(vertex_text);

    /* check vertex shader */
    glGetShaderiv(out->vert, GL_COMPILE_STATUS, &result);
    glGetShaderiv(out->vert, GL_INFO_LOG_LENGTH, &log_size);
    log_size_max = log_size;
    log = (char *)malloc(((log_size > 1) ? log_size : 1) * sizeof(char));
    if(!result) {
        glGetShaderInfoLog(out->vert, log_size, NULL, log);
        fprintf(stderr, "[Shader INFO] '%s' Vertex Log: %s", vertex_path, log);
    }

    /* compile fragment shader */
    snprintf(filepath_buf, sizeof(filepath_buf), DATADIR "/shaders/%s", fragment_path);
    fs = file_read_to_buffer(&fragment_text, filepath_buf);
    if(fs == -1) {
        fprintf(stderr, "[Shader ERR] %s\n", "No such fragment shader file");
        return -1;
    }
    out->frag = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(out->frag, 1, (const char *const *)&fragment_text, NULL);
    glCompileShader(out->frag);

    free(fragment_text);

    /* check fragment shader */
    glGetShaderiv(out->frag, GL_COMPILE_STATUS, &result);
    glGetShaderiv(out->frag, GL_INFO_LOG_LENGTH, &log_size);
    if(log_size > log_size_max) {
        log_size_max = log_size;
        log = realloc(log, log_size*sizeof(char));
    }
    if(!result) {
        glGetShaderInfoLog(out->frag, log_size, NULL, log);
        fprintf(stderr, "[Shader INFO] '%s' Fragment Log: %s", fragment_path, log);
    }

    /* create program */
    out->program = glCreateProgram();
    glAttachShader(out->program, out->vert);
    glAttachShader(out->program, out->frag);
    glLinkProgram(out->program);

    /* check program */
    glGetProgramiv(out->program, GL_LINK_STATUS, &result);
    glGetProgramiv(out->program, GL_INFO_LOG_LENGTH, &log_size);
    if(log_size > log_size_max) {
        log_size_max = log_size;
        log = realloc(log, log_size);
    }
    if(!result) {
        glGetProgramInfoLog(out->program, log_size, NULL, log);
        fprintf(stderr, "[Shader INFO] Program Log: %s", log);
    }
    free(log);

    glDeleteShader(out->vert);
    glDeleteShader(out->frag);

    return 0;
}

void shader_use(shader_t *shader) {
    glUseProgram(shader->program);
}

void shader_stop() {
    glUseProgram(0);
}

/* atlas/texture */
int atlas_create_png(texture_atlas_t *out, const char *filepath) {
    char filepath_buf[256];
    png_structp png_ptr;
    png_infop info_ptr;
    int transforms;
    size_t row_bytes;
    png_bytepp row_pointers;
    FILE *fp;

    snprintf(filepath_buf, sizeof(filepath_buf), DATADIR "/textures/%s", filepath);
    if((fp = fopen(filepath_buf, "rb")) == NULL)
        return -1;

    if((png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING,
                                         NULL, NULL, NULL)) == NULL) {
        fclose(fp);
        return -1;
    }

    if((info_ptr = png_create_info_struct(png_ptr)) == NULL) {
        png_destroy_read_struct(&png_ptr, NULL, NULL);
        fclose(fp);
        return -1;
    }

    if(setjmp(png_jmpbuf(png_ptr))) {
        png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
        fclose(fp);
        return -1;
    }

    png_init_io(png_ptr, fp);
    transforms = PNG_TRANSFORM_STRIP_16 | PNG_TRANSFORM_PACKING
        | PNG_TRANSFORM_EXPAND;
    png_read_png(png_ptr, info_ptr, transforms, NULL);
    png_get_IHDR(png_ptr, info_ptr, &out->w, &out->h, NULL, NULL,
                 NULL, NULL, NULL);

    row_bytes = png_get_rowbytes(png_ptr, info_ptr);
    row_pointers = png_get_rows(png_ptr, info_ptr);
    out->bytes = malloc(row_bytes * out->h);

    for(uint32_t i = 0; i < out->h; i++)
        memcpy(out->bytes + row_bytes*(out->h-1 - i),
               row_pointers[i], row_bytes);

    png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
    fclose(fp);

    return 0;
}

void atlas_free(texture_atlas_t *atlas) {
    free(atlas->bytes);
}

void texture_create(texture_t *out, texture_atlas_t *atlas,
                    uint32_t xmin, uint32_t ymin,
                    uint32_t xmax, uint32_t ymax) {
    out->atlas = atlas;
    out->w = xmax - xmin;
    out->h = ymax - ymin;

    float left   = (float)xmin / atlas->w,
          bottom = (float)ymin / atlas->h,
          right  = (float)xmax / atlas->w,
          top    = (float)ymax / atlas->h;
    out->uv[0].vec.x = left;
    out->uv[0].vec.y = bottom;
    out->uv[1].vec.x = right;
    out->uv[1].vec.y = bottom;
    out->uv[2].vec.x = left;
    out->uv[2].vec.y = top;
    out->uv[3].vec.x = right;
    out->uv[3].vec.y = top;
}

int texture_create_sheet(texture_t **out, texture_atlas_t *atlas,
                         uint32_t xmin, uint32_t ymin,
                         uint32_t xmax, uint32_t ymax,
                         uint32_t w, uint32_t h) {
    uint32_t rows = (xmax - xmin) / w,
             cols = (ymax - ymin) / h;

    if(rows == 0 || cols == 0)
        return -1;

    *out = malloc(sizeof(texture_t) * rows*cols);
    for(uint32_t i = 0; i < rows*cols; i++) {
        uint32_t x = w * (i % rows),
                 y = atlas->h - h * (i/rows);
        texture_create(*out+i, atlas, x, y-h, x+w, y);
    }

    return 0;
}

void texture_destroy_sheet(texture_t **sheet) {
    free(*sheet);
}

/* font */
void font_init() {
    if(shader_load_from_file(&s.shaders.font_shader, "font.vert", "font.frag"))
        fprintf(stderr, "Failed to load font_shader\n");
}

int font_create(texture_t out[128], texture_atlas_t *atlas, uint32_t char_w, uint32_t char_h) {
    uint8_t rows = atlas->w / char_w,
            cols = atlas->h / char_h;

    if(rows == 0 || cols == 0 || rows*cols < 128)
        return -1;

    for(uint8_t i = 0; i < 128; i++) {
        uint32_t x = char_w * (i % rows),
                 y = atlas->h - char_h * (i / rows);
        texture_create(out+i, atlas, x, y-char_h, x+char_w, y);
    }

    return 0;
}

void text_create(text_sprite_t *out, font_t *font, char *str) {
    out->font = font;
    strncpy(out->str, str, sizeof(out->str));
    glGenBuffers(2, &out->_vbo);

    out->_len = strlen(out->str);
    out->_vertices = malloc(4*out->_len * sizeof(texture_vertex_t));
    out->_indices  = malloc(6*out->_len * sizeof(GLushort));

    /* default values */
    out->pos = (vec2i){ 0, 0, };
    out->scale = 1.f;
    for(int i = 0; i < 4; i++)
        out->color.arr[i] = 1.f;
}

void text_update(text_sprite_t *text, bool tile_pos) {
    size_t len = strlen(text->str);
    /* NOTE: jank way to remove invisible chars from length */
    for(char *c = text->str; *c != '\0'; c++)
        if(*c == '\n') --len;

    if(len > text->_len) {
        text->_vertices = realloc(text->_vertices, 4*len * sizeof(texture_vertex_t));
        text->_indices  = realloc(text->_indices,  6*len * sizeof(GLushort));
    }
    if(len != text->_len) {
        text->_len = len;
    }

    char c; int32_t x_off = 0, y_off = 0, i_off = 0;
    for(int i = 0; (c = text->str[i+i_off]) != '\0'; i++) {
        if(c > 127) c = 0;
        else if(c == '\n') {
            x_off = 0;
            if(tile_pos)
                y_off -= (int)text->scale;
            else
                y_off -= (*text->font)[c].h * text->scale;
            --i; ++i_off;
            continue;
        }

        for(int j = 0; j < 4; j++) {
            text->_vertices[4*i+j].u = (*text->font)[c].uv[j].vec.x;
            text->_vertices[4*i+j].v = (*text->font)[c].uv[j].vec.y;
        }

        float normal_x, normal_y, normal_w, normal_h;
        if(tile_pos) {
            normal_x = (float)(text->pos.x + x_off) / TILEMAP_WIDTH;
            normal_y = (float)(text->pos.y + y_off) / TILEMAP_HEIGHT;
            normal_w = TILEMAP_NORM_W * (int)text->scale;
            normal_h = TILEMAP_NORM_H * (int)text->scale;
        } else {
            normal_x = (float)(text->pos.x + x_off) / s.gfx.width;
            normal_y = (float)(text->pos.y + y_off) / s.gfx.height;
            normal_w = (float)(*text->font)[c].w    / s.gfx.width  * text->scale;
            normal_h = (float)(*text->font)[c].h    / s.gfx.height * text->scale;
        }

        text->_vertices[4*i+0].x = normal_x;
        text->_vertices[4*i+0].y = normal_y;
        text->_vertices[4*i+1].x = normal_x + normal_w;
        text->_vertices[4*i+1].y = normal_y;
        text->_vertices[4*i+2].x = normal_x;
        text->_vertices[4*i+2].y = normal_y + normal_h;
        text->_vertices[4*i+3].x = normal_x + normal_w;
        text->_vertices[4*i+3].y = normal_y + normal_h;

        if(tile_pos)
            x_off += (int)text->scale;
        else
            x_off += (*text->font)[c].w * text->scale;

        text->_indices[6*i+0] = 4*i + 0;
        text->_indices[6*i+1] = 4*i + 1;
        text->_indices[6*i+2] = 4*i + 2;
        text->_indices[6*i+3] = 4*i + 1;
        text->_indices[6*i+4] = 4*i + 2;
        text->_indices[6*i+5] = 4*i + 3;
    }

    glBindBuffer(GL_ARRAY_BUFFER, text->_vbo);
    glBufferData(GL_ARRAY_BUFFER, len*4*sizeof(texture_vertex_t), text->_vertices, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, text->_ibo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, len*6*sizeof(GLushort), text->_indices, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void text_render_begin() {
    shader_use(&s.shaders.font_shader);
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
}

void text_render_end() {
    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    shader_stop();
}

void text_render(text_sprite_t *text) {
    glBindBuffer(GL_ARRAY_BUFFER, text->_vbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, text->_ibo);

    /* xy */
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE,
                          sizeof(texture_vertex_t), (void *)0);
    /* uv */
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE,
                          sizeof(texture_vertex_t), (void *)(sizeof(float)*2));

    /* color */
    glUniform4fv(2, 1, (const GLfloat *)text->color.arr);

    glDrawElements(GL_TRIANGLES, text->_len*6*sizeof(GLushort), GL_UNSIGNED_SHORT, (void *)0);
}

void text_destroy(text_sprite_t *text) {
    free(text->_vertices);
    free(text->_indices);
}

/* sprite */
const GLushort sprite_indices[6] = {
    0, 1, 2,
    1, 2, 3,
};
GLuint sprite_ibo;
void sprite_init() {
    glGenBuffers(1, &sprite_ibo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, sprite_ibo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sprite_indices), sprite_indices, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    if(shader_load_from_file(&s.shaders.sprite_shader, "sprite.vert", "sprite.frag"))
        fprintf(stderr, "Failed to load sprite_shader\n");
}

void sprite_create(sprite_t *out, texture_t *texture) {
    out->tex = texture;
    glGenBuffers(1, &out->_vbo);

    /* default values */
    out->pos = (vec2i){ 0, 0, };
    out->scale = 1.f;
    out->rot = DIR_UP;
}

void sprite_update(sprite_t *sprite) {
    float normal_x = (float)sprite->pos.x  / s.gfx.width,
          normal_y = (float)sprite->pos.y  / s.gfx.height,
          normal_w = (float)sprite->tex->w / s.gfx.width * sprite->scale,
          normal_h = (float)sprite->tex->h / s.gfx.height * sprite->scale;

    sprite->_vertices[0].x = normal_x;
    sprite->_vertices[0].y = normal_y;
    sprite->_vertices[1].x = normal_x + normal_w;
    sprite->_vertices[1].y = normal_y;
    sprite->_vertices[2].x = normal_x;
    sprite->_vertices[2].y = normal_y + normal_h;
    sprite->_vertices[3].x = normal_x + normal_w;
    sprite->_vertices[3].y = normal_y + normal_h;

    for(int i = 0; i < 4; i++) {
        sprite->_vertices[rotate_vertex(i, sprite->rot)].u = sprite->tex->uv[i].vec.x;
        sprite->_vertices[rotate_vertex(i, sprite->rot)].v = sprite->tex->uv[i].vec.y;
    }

    glBindBuffer(GL_ARRAY_BUFFER, sprite->_vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sprite->_vertices), sprite->_vertices, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void sprite_render_begin() {
    shader_use(&s.shaders.sprite_shader);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, sprite_ibo);
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
}

void sprite_render_end() {
    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    shader_stop();
}

void sprite_render(sprite_t *sprite) {
    glBindBuffer(GL_ARRAY_BUFFER, sprite->_vbo);

    /* xy */
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE,
                          sizeof(texture_vertex_t), (void *)0);
    /* uv */
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE,
                          sizeof(texture_vertex_t), (void *)(sizeof(float)*2));

    glDrawElements(GL_TRIANGLES, sizeof(sprite_indices), GL_UNSIGNED_SHORT, (void *)0);
}
