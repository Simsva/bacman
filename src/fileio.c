#include "fileio.h"
#include "config.h"

#ifdef _WIN32
/* # include <Windows.h> */
#else
// Assume POSIX
# include <sys/stat.h>
#endif

#include <stdlib.h>
#include <stdio.h>

size_t file_get_size(FILE *fp) {
    size_t fs;
#ifdef _WIN32
    // TODO: Win32 API

    fseek(fp, 0L, SEEK_END);
    fs = ftell(fp);
    rewind(fp);
#else /* _WIN32 */
    // Assume POSIX

    struct stat st;
    fstat(fileno(fp), &st);
    fs = st.st_size;
#endif /* _WIN32 */
    return fs;
}

size_t file_read_to_buffer(char **out, const char *filepath) {
    size_t fs;
    FILE *fp;

    fp = fopen(filepath, "rb");
    if(!fp)
        return -1;

    fs = file_get_size(fp);
    *out = (char*)calloc(fs+1, sizeof(char));
    fread(*out, sizeof(char), fs, fp);
    (*out)[fs] = '\0';
    fclose(fp);

    return fs+1;
}
