#include "game.h"

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>
#include <limits.h>

#include "game_types.h"
#include "fileio.h"
#include "shaders.h"
#include "game_logic.h"

void tilemap_init(void);
void tilemap_render(void);
bool can_move(vec2i pos, dir_t dir);
vec2i move(vec2i pos, dir_t dir);
void pacman_controls(dir_t wanted_dir);
int ghost_current_speed(const ghost_t *ghost);
ghost_state_t ghost_scatter_chase(void);
void ghost_update_state(ghost_t *ghost);
void ghost_update_sprite(ghost_t *ghost);
void ghost_update_target(ghost_t *ghost);
bool ghost_update_dir(ghost_t *ghost);

enum {
    TILE_EMPTY = 0x00,
    TILE_DOT = 0x01,
    TILE_PILL = 0x02,
};

/* ghost stuff */
static const vec2i ghost_scatter_targets[NUM_GHOSTS] = {
    { 25, 33, }, { 2, 36, }, { 27, 2, }, { 0, 2, },
};
static const vec2i ghost_start_pos[NUM_GHOSTS] = {
    { 14*8, 22*8+4, }, { 14*8, 19*8+4, }, { 12*8, 19*8+4, }, { 16*8, 19*8+4, },
};

texture_atlas_t atlas, font;

GLuint tilemap_vbo, tilemap_ibo;
texture_vertex_t tilemap_vertices[TILEMAP_WIDTH*(TILEMAP_HEIGHT-6)*4];
GLushort tilemap_indices[TILEMAP_WIDTH*(TILEMAP_HEIGHT-6)*6];
void tilemap_init(void) {
    /* ripped off from https://github.com/floooh/pacman.c */
    static const char* tiles =
    /*             111111111122222222 */
    /*   0123456789012345678901234567 */
        "0UUUUUUUUUUUU45UUUUUUUUUUUU1"  // 0
        "L............rl............R"  // 1
        "L.ebbf.ebbbf.rl.ebbbf.ebbf.R"  // 2
        "LPr  l.r   l.rl.r   l.r  lPR"  // 3
        "L.guuh.guuuh.gh.guuuh.guuh.R"  // 4
        "L..........................R"  // 5
        "L.ebbf.ef.ebbbbbbf.ef.ebbf.R"  // 6
        "L.guuh.rl.guuyxuuh.rl.guuh.R"  // 7
        "L......rl....rl....rl......R"  // 8
        "2BBBBf.rzbbf rl ebbwl.eBBBB3"  // 9
        "     L.rxuuh gh guuyl.R     "  // 10
        "     L.rl          rl.R     "  // 11
        "     L.rl mjs--tjn rl.R     "  // 12
        "UUUUUh.gh i      q gh.gUUUUU"  // 13
        "      .   i      q   .      "  // 14
        "BBBBBf.ef i      q ef.eBBBBB"  // 15
        "     L.rl okkkkkkp rl.R     "  // 16
        "     L.rl          rl.R     "  // 17
        "     L.rl ebbbbbbf rl.R     "  // 18
        "0UUUUh.gh guuyxuuh gh.gUUUU1"  // 19
        "L............rl............R"  // 20
        "L.ebbf.ebbbf.rl.ebbbf.ebbf.R"  // 21
        "L.guyl.guuuh.gh.guuuh.rxuh.R"  // 22
        "LP..rl.......  .......rl..PR"  // 23
        "6bf.rl.ef.ebbbbbbf.ef.rl.eb8"  // 24
        "7uh.gh.rl.guuyxuuh.rl.gh.gu9"  // 25
        "L......rl....rl....rl......R"  // 26
        "L.ebbbbwzbbf.rl.ebbwzbbbbf.R"  // 27
        "L.guuuuuuuuh.gh.guuuuuuuuh.R"  // 28
        "L..........................R"  // 29
        "2BBBBBBBBBBBBBBBBBBBBBBBBBB3"; // 30
    /*             111111111122222222 */
    /*   0123456789012345678901234567 */

    uint8_t t[128];
    for(uint8_t i = 0; i < 128; i++) t[i] = TILE_DOT;
    t[' ']=TILE_EMPTY; t['0']=0x16; t['1']=0x13; t['2']=0x15; t['3']=0x14;
    t['4']=0x17; t['5']=0x18; t['6']=0x1e; t['7']=0x1d; t['8']=0x19;
    t['9']=0x1a; t['U']=0x0f; t['L']=0x10; t['R']=0x12; t['B']=0x11;
    t['b']=0x05; t['e']=0x0a; t['f']=0x07; t['g']=0x09; t['h']=0x08;
    t['l']=0x04; t['r']=0x06; t['u']=0x03; t['w']=0x0c; t['x']=0x0e;
    t['y']=0x0b; t['z']=0x0d; t['m']=0x22; t['n']=0x1f; t['o']=0x21;
    t['p']=0x20; t['j']=0x11; t['i']=0x12; t['k']=0x0f; t['q']=0x10;
    t['s']=0x23; t['t']=0x24; t['-']=0x25; t['P']=TILE_PILL;
    for(int y = TILEMAP_HEIGHT-1-6, i = 0; y >= 0; y--)
        for(int x = 0; x < TILEMAP_WIDTH; x++, i++)
            s.gfx.tilemap[y][x] = t[tiles[i] & 0xff];
}

void tilemap_render(void) {
    int i = 0;
    for(int y = 3; y < TILEMAP_HEIGHT-3; y++)
        for(int x = 0; x < TILEMAP_WIDTH; x++) {
            uint8_t tile = s.gfx.tilemap[y-3][x];
            if(tile) {
                tilemap_vertices[4*i+0].x = TILEMAP_NORM_W * x;
                tilemap_vertices[4*i+0].y = TILEMAP_NORM_H * y;
                tilemap_vertices[4*i+1].x = TILEMAP_NORM_W * (x+1);
                tilemap_vertices[4*i+1].y = TILEMAP_NORM_H * y;
                tilemap_vertices[4*i+2].x = TILEMAP_NORM_W * x;
                tilemap_vertices[4*i+2].y = TILEMAP_NORM_H * (y+1);
                tilemap_vertices[4*i+3].x = TILEMAP_NORM_W * (x+1);
                tilemap_vertices[4*i+3].y = TILEMAP_NORM_H * (y+1);

                for(int j = 0; j < 4; j++) {
                    tilemap_vertices[4*i+j].u = s.textures.tiles[tile-1].uv[j].vec.x;
                    tilemap_vertices[4*i+j].v = s.textures.tiles[tile-1].uv[j].vec.y;
                }

                tilemap_indices[6*i+0] = 4*i+0;
                tilemap_indices[6*i+1] = 4*i+1;
                tilemap_indices[6*i+2] = 4*i+2;
                tilemap_indices[6*i+3] = 4*i+1;
                tilemap_indices[6*i+4] = 4*i+2;
                tilemap_indices[6*i+5] = 4*i+3;

                i++;
            }
        }

    shader_use(&s.shaders.sprite_shader);
    glBindBuffer(GL_ARRAY_BUFFER, tilemap_vbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, tilemap_ibo);
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    glBufferData(GL_ARRAY_BUFFER, sizeof(texture_vertex_t) * 4*i, tilemap_vertices, GL_STATIC_DRAW);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLushort) * 6*i, tilemap_indices, GL_STATIC_DRAW);

    /* xy */
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE,
                          sizeof(texture_vertex_t), (void *)0);
    /* uv */
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE,
                          sizeof(texture_vertex_t), (void *)(sizeof(float)*2));

    glDrawElements(GL_TRIANGLES, sizeof(GLushort) * 6*i, GL_UNSIGNED_SHORT, (void *)0);

    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    shader_stop();
}

bool can_move(vec2i pos, dir_t dir) {
    const vec2i dir_vec = dir_to_vec2i(dir),
                mid_d = dist_to_tile_mid(pos),
                tilemap_pos = pixel_to_tile_pos(pos),
                check_pos = tilemap_clamp(vec2i_add(tilemap_pos, dir_vec));
    int32_t mid_d_front, mid_d_side;

    if(dir & 1) {
        mid_d_front = mid_d.x;
        mid_d_side  = mid_d.y;
    } else {
        mid_d_front = mid_d.y;
        mid_d_side  = mid_d.x;
    }

    uint8_t tile = s.gfx.tilemap[check_pos.y-3][check_pos.x];
    bool tile_blocked = tile != TILE_EMPTY && tile != TILE_DOT && tile != TILE_PILL,
         movement_blocked = (mid_d_side != 0) || (tile_blocked && (mid_d_front == 0));
    return !movement_blocked;
}

vec2i move(vec2i pos, dir_t dir) {
    const vec2i dir_vec = dir_to_vec2i(dir);
    pos = vec2i_add(pos, dir_vec);

    /* center sideways position */
    /* NOTE: unneccesary since cutting corners is disabled */
    /* const vec2i mid_d = dist_to_tile_mid(pos); */
    /* if(dir & 1) */
    /*     pos.y -= mid_d.y; */
    /* else */
    /*     pos.x -= mid_d.x; */

    /* wrapping */
    if(pos.x < 0)
        pos.x = TILEMAP_PIXEL_W-1;
    else if(pos.x > TILEMAP_PIXEL_W-1)
        pos.x = 0;
    return pos;
}

void pacman_controls(dir_t wanted_dir) {
    actor_t *a = &s.game.pacman;
    vec2i tile_pos = tilemap_clamp(pixel_to_tile_pos(a->pixel_pos));
    uint8_t *tile = &s.gfx.tilemap[tile_pos.y-3][tile_pos.x];

    switch(*tile) {
    case TILE_DOT:
        *tile = TILE_EMPTY;
        s.game.score += 10;
        break;

    case TILE_PILL:
        *tile = TILE_EMPTY;
        s.game.score += 50;

        s.game.ghosts_eaten = 0;
        for(int i = 0; i < NUM_GHOSTS; i++) {
            ghost_t *ghost = s.game.ghosts+i;
            switch(ghost->state) {
            case GHOST_STATE_CHASE:
            case GHOST_STATE_SCATTER:
            case GHOST_STATE_FRIGHTENED:
                trigger_start(&ghost->frightened);
            default:
                break;
            }
        }
        break;
    }

    /* ghost collision */
    for(int i = 0; i < NUM_GHOSTS; i++) {
        ghost_t *ghost = s.game.ghosts+i;
        if(vec2i_equal(pixel_to_tile_pos(ghost->actor.pixel_pos), tile_pos)) {
            switch(ghost->state) {
            /* eat */
            case GHOST_STATE_FRIGHTENED:
                ghost->state = GHOST_STATE_EYES;
                trigger_start_delay(&ghost->eaten, FREEZE_GHOST_EATEN);

                s.game.score += 100 * (1<<++s.game.ghosts_eaten);
                s.game.freeze_ticks += FREEZE_GHOST_EATEN;
                break;

            /* die */
            case GHOST_STATE_CHASE:
            case GHOST_STATE_SCATTER:
                if(!s.debug.godmode) {
                    if(--s.game.lives > 0) {
                        uint64_t delay = FREEZE_PACMAN_EATEN;
                        s.game.freeze_ticks += delay;
                        trigger_start_delay(&s.game.round_started, delay+1);
                    } else
                        trigger_start(&s.game.game_over);
                }
                break;

            /* do nothing */
            default:
                break;
            }
        }
    }

    if(can_move(a->pixel_pos, wanted_dir))
        a->dir = a->point_dir = wanted_dir;
    if(can_move(a->pixel_pos, a->dir))
        a->pixel_pos = move(a->pixel_pos, a->dir);
}

/* returns amount of pixels for a ghost to move this tick */
int ghost_current_speed(const ghost_t *ghost) {
    switch(ghost->state) {
    /* half speed inside house and when frightened (1 every other tick) */
    case GHOST_STATE_HOUSE:
    case GHOST_STATE_LEAVEHOUSE:
    case GHOST_STATE_FRIGHTENED:
        return s.tick & 1;

    /* 1.5x speed after being eaten (alternate between 1 and 2 pixels/tick) */
    case GHOST_STATE_EYES:
    case GHOST_STATE_ENTERHOUSE:
        return (s.tick & 1) ? 1 : 2;

    /* move a bit slower than Pac-Man otherwise */
    default:
        return (s.tick % 7) ? 1 : 0;
    }
}

/* returns chase or scatter state depending on tick */
ghost_state_t ghost_scatter_chase(void) {
    uint64_t t = trigger_since(s.game.round_started);
    if(t < 7*TARGET_TPS)       return GHOST_STATE_SCATTER;
    else if(t < 27*TARGET_TPS) return GHOST_STATE_CHASE;
    else if(t < 34*TARGET_TPS) return GHOST_STATE_SCATTER;
    else if(t < 54*TARGET_TPS) return GHOST_STATE_CHASE;
    else if(t < 59*TARGET_TPS) return GHOST_STATE_SCATTER;
    else if(t < 79*TARGET_TPS) return GHOST_STATE_CHASE;
    else if(t < 84*TARGET_TPS) return GHOST_STATE_SCATTER;
    else                       return GHOST_STATE_CHASE;
}

void ghost_update_state(ghost_t *ghost) {
    ghost_state_t state = ghost->state;

    switch(ghost->state) {
    /* ghosts are only in house after new round */
    case GHOST_STATE_HOUSE:
        if(trigger_since(s.game.force_leave_house) == 4*TARGET_TPS) {
            state = GHOST_STATE_LEAVEHOUSE;
            trigger_start(&s.game.force_leave_house);
        }
        /* TODO: dot counter */
        break;

    /* ghosts always leave the house immediately, unless they are in
    ** GHOST_STATE_HOUSE at the beginning of the round */
    case GHOST_STATE_ENTERHOUSE:
    {
        /* use Pinky's starting position for Blinky */
        const vec2i pos = ghost->actor.pixel_pos,
                    target_pos = ghost_start_pos[ghost->type == GHOST_TYPE_BLINKY
                                                 ? GHOST_TYPE_PINKY
                                                 : ghost->type];

        if(vec2i_near(pos, target_pos, 1)) {
            state = GHOST_STATE_LEAVEHOUSE;
            trigger_disable(&ghost->frightened);
        }
    }
        break;

    case GHOST_STATE_LEAVEHOUSE:
        if(ghost->actor.pixel_pos.y == 22*TILE_HEIGHT+TILE_HEIGHT/2) {
            state = GHOST_STATE_CHASE;
            ghost->next_dir = ghost->actor.dir = DIR_LEFT;
        }
        break;

    case GHOST_STATE_EYES:
        if(vec2i_near(ghost->actor.pixel_pos,
                      (vec2i){ TILEMAP_DOOR_X, TILEMAP_DOOR_Y, }, 1))
            state = GHOST_STATE_ENTERHOUSE;
        break;

    default:
        state = trigger_before(ghost->frightened, TICK_FRIGHT)
            ? GHOST_STATE_FRIGHTENED
            : ghost_scatter_chase();
        break;
    }

    if(ghost->state != state) {
        switch(ghost->state) {
        /* reverse direction when transitioning from chase or scatter */
        case GHOST_STATE_CHASE:
        case GHOST_STATE_SCATTER:
            ghost->next_dir = opposite_dir(ghost->actor.dir);
        default:
            break;
        }
        ghost->state = state;
    }
}

void ghost_update_sprite(ghost_t *ghost) {
    sprite_t *spr = &ghost->actor.sprite;
    switch(ghost->state) {
    case GHOST_STATE_EYES:
    case GHOST_STATE_ENTERHOUSE:
        spr->tex = &s.textures.ghost_eyes;
        break;

    case GHOST_STATE_FRIGHTENED:
        spr->tex = &s.textures.ghost_frightened;
        break;

    default:
        spr->tex = s.textures.ghost_tex+ghost->type;
        break;
    }
}

void ghost_update_target(ghost_t *ghost) {
    vec2i target = ghost->target;

    switch(ghost->state) {
    /* in scatter mode all ghost head to their own pre-defined scatter point */
    case GHOST_STATE_SCATTER:
        target = ghost_scatter_targets[ghost->type];
        break;

    /* in chase mode all ghosts use their own methods of chasing Pac-Man */
    case GHOST_STATE_CHASE:
    {
        const actor_t *pacman = &s.game.pacman;
        const vec2i pacman_pos = pixel_to_tile_pos(pacman->pixel_pos),
                    pacman_dir = dir_to_vec2i(pacman->dir);

        /* all ghost types have different chase behaviours */
        switch(ghost->type) {
        /* Blinky chases Pac-Man */
        case GHOST_TYPE_BLINKY:
            target = pacman_pos;
            break;

        /* Pinky targets 4 tiles ahead of Pac-Man */
        case GHOST_TYPE_PINKY:
            target = vec2i_add(pacman_pos, vec2i_scale(pacman_dir, 4));
            break;

        /* Inky targets the opposite side of a point 2 tiles in front of Pac-Man
        ** from Blinky
        **
        ** If `T` is the target, `P` Pac-Man and `B` Blinky, then:
        **
        ** B--+--T
        **    |
        **    P */
        case GHOST_TYPE_INKY:
        {
            const vec2i blinky_pos = pixel_to_tile_pos(
                            s.game.ghosts[GHOST_TYPE_BLINKY].actor.pixel_pos
                        ),
                        point = vec2i_add(pacman_pos, vec2i_scale(pacman_dir, 2)),
                        dist = vec2i_sub(point, blinky_pos);
            target = vec2i_add(blinky_pos, vec2i_scale(dist, 2));
        }
            break;

        /* if Clyde is near Pac-Man he chases him, otherwise he goes targets his
        ** scatter point */
        case GHOST_TYPE_CLYDE:
        {
            const vec2i clyde_pos = pixel_to_tile_pos(
                            s.game.ghosts[GHOST_TYPE_CLYDE].actor.pixel_pos
                        );
            target = (vec2i_sqrdist(clyde_pos, pacman_pos) < 64)
                ? pacman_pos
                : ghost_scatter_targets[GHOST_TYPE_CLYDE];
        }
            break;

        default:
            break;
        }
    }
        break;

    /* when frightened all ghosts target a random point, i.e. they just move
    ** around randomly */
    case GHOST_STATE_FRIGHTENED:
        target = (vec2i){
            rand() % TILEMAP_WIDTH,
            rand() % TILEMAP_HEIGHT,
        };
        break;

    /* in eyes mode all ghosts move towards the door */
    case GHOST_STATE_EYES:
        target = (vec2i){ 13, 22 };
        break;

    default:
        break;
    }

    ghost->target = target;
}

/* returns true if can_move should be ignored */
bool ghost_update_dir(ghost_t *ghost) {
    switch(ghost->state) {
    /* move up and down */
    case GHOST_STATE_HOUSE:
        if(ghost->actor.pixel_pos.y <= 19*TILE_HEIGHT)
            ghost->next_dir = DIR_UP;
        else if(ghost->actor.pixel_pos.y >= 20*TILE_HEIGHT)
            ghost->next_dir = DIR_DOWN;

        ghost->actor.dir = ghost->next_dir;
        return true;

    case GHOST_STATE_LEAVEHOUSE:
    {
        const vec2i pos = ghost->actor.pixel_pos,
            outside_pos = { .x = 14*TILE_WIDTH, .y = 22*TILE_HEIGHT+TILE_HEIGHT/2 };
        if(pos.x == outside_pos.x && pos.y < outside_pos.y)
            ghost->next_dir = DIR_UP;
        else {
            const int32_t house_y = 19*TILE_HEIGHT+TILE_HEIGHT/2;
            ghost->next_dir = (pos.y > house_y)
                ? DIR_DOWN
                : (pos.y < house_y)
                ? DIR_UP
                : (pos.x > outside_pos.x)
                ? DIR_LEFT
                : DIR_RIGHT;
        }

        ghost->actor.dir = ghost->next_dir;
        return true;
    }

    /* enter house (don't navigate to house, just enter through the door) */
    case GHOST_STATE_ENTERHOUSE:
    {
        const vec2i pos = ghost->actor.pixel_pos,
                    tile_pos = pixel_to_tile_pos(pos),
                    /* use Pinky's starting position for Blinky */
                    target_pos = ghost_start_pos[ghost->type == GHOST_TYPE_BLINKY
                                                 ? GHOST_TYPE_PINKY
                                                 : ghost->type];

        const vec2i outside_pos = {
            .x = 14*TILE_WIDTH,
            .y = 22*TILE_WIDTH*TILE_HEIGHT/2
        };
        /* if outside house */
        if(tile_pos.y == 22) {
            if(pos.x != outside_pos.x)
                ghost->next_dir = (pos.x < outside_pos.x) ? DIR_RIGHT : DIR_LEFT;
            else
                ghost->next_dir = DIR_DOWN;
        } else if(pos.y == target_pos.y)
            ghost->next_dir = (pos.x < target_pos.x) ? DIR_RIGHT : DIR_LEFT;

        ghost->actor.dir = ghost->next_dir;
        return true;
    }

    /* for all other states, navigate to the set target point (without forcing
    ** movement of course) */
    default:
    {
        /* only change directions when in the center of a tile */
        const vec2i mid_d = dist_to_tile_mid(ghost->actor.pixel_pos);
        if((mid_d.x == 0) && (mid_d.y == 0)) {
            /* ghosts think one tile in the future */
            ghost->actor.dir = ghost->next_dir;

            const vec2i dir_vec = dir_to_vec2i(ghost->actor.dir),
                        next_pos = vec2i_add(
                            pixel_to_tile_pos(ghost->actor.pixel_pos), dir_vec);

            /* try all directions and choose the one that gets the ghost closest
            ** to its target */
            uint32_t min_dist = INT_MAX, dist = 0;
            for(int i = 0; i < NUM_DIRS; i++) {
                const dir_t dir = (dir_t)i;

                /* TODO: implement redzones */
                bool redzone = false;
                /* disallow upwards movement in redzones and reversing everywhere */
                if((redzone && (dir == DIR_UP) && (ghost->state != GHOST_STATE_EYES))
                   || (opposite_dir(dir) == ghost->actor.dir))
                    continue;

                const vec2i test_pos = tilemap_clamp(
                    vec2i_add(next_pos, dir_to_vec2i(dir)));
                const uint8_t tile = s.gfx.tilemap[test_pos.y-3][test_pos.x];
                bool tile_blocked =
                    tile != TILE_EMPTY && tile != TILE_DOT && tile != TILE_PILL;
                if(!tile_blocked
                   && (dist = vec2i_sqrdist(test_pos, ghost->target)) < min_dist) {
                    min_dist = dist;
                    ghost->next_dir = dir;
                }
            }
        }
    }
        return false;
    }
}

text_sprite_t debug_text, score_text, lives_text,
    game_over_text, ghost_debug_text[NUM_GHOSTS];
bool show_game_over = false;
void game_init() {
    /* initialization */

    /* force aspect ratio */
    s.gfx.ratio = (float)TILEMAP_WIDTH/TILEMAP_HEIGHT;
    s.game.freeze_ticks = 0;

    /* debug options */
    s.debug.show_debug = false;
    s.debug.show_ghost_debug = false;
    s.debug.dump_pos = false;
    s.debug.godmode = false;

    /* GL settings */
    glShadeModel(GL_FLAT);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    /* initialize stuff */
    sprite_init();
    font_init();


    /* textures */
    {
        GLuint tex_bufs[2];
        glGenTextures(sizeof(tex_bufs)/sizeof(GLuint), tex_bufs);

        /* non-font textures */
        if(atlas_create_png(&atlas, "atlas.png")) {
            fprintf(stderr, "Failed to load texture atlas\n");
            exit(-1);
        }
        texture_create_sheet(&s.textures.tiles, &atlas,
                             0, 32, 80, 64, TILE_WIDTH, TILE_HEIGHT);

        /* non-tile textures */
        texture_create(&s.textures.pacman_tex, &atlas, 0, 16, 16, 32);
        texture_create(s.textures.ghost_tex+GHOST_TYPE_BLINKY,
                    &atlas, 16, 16, 32, 32);
        texture_create(s.textures.ghost_tex+GHOST_TYPE_PINKY,
                    &atlas, 32, 16, 48, 32);
        texture_create(s.textures.ghost_tex+GHOST_TYPE_INKY,
                    &atlas, 48, 16, 64, 32);
        texture_create(s.textures.ghost_tex+GHOST_TYPE_CLYDE,
                    &atlas, 64, 16, 80, 32);
        texture_create(&s.textures.ghost_eyes, &atlas, 16, 0, 32, 16);
        texture_create(&s.textures.ghost_frightened, &atlas, 0, 0, 16, 16);

        glActiveTexture(GL_TEXTURE0 + 0);
        glBindTexture(GL_TEXTURE_2D, tex_bufs[0]);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, atlas.w, atlas.h, 0, GL_RGBA, GL_UNSIGNED_BYTE, atlas.bytes);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        atlas_free(&atlas);

        /* font */
        if(atlas_create_png(&font, "font.png")) {
            fprintf(stderr, "Failed to load font atlas\n");
            exit_safe(-1);
        }
        font_create(s.textures.font, &font, 7, 9);

        glActiveTexture(GL_TEXTURE0 + 1);
        glBindTexture(GL_TEXTURE_2D, tex_bufs[1]);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, font.w, font.h, 0, GL_RGBA, GL_UNSIGNED_BYTE, font.bytes);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        atlas_free(&font);
    }


    /* text objects */

    /* debug text */
    text_create(&debug_text, &s.textures.font, "FPS:0000\ndumppos:off");
    for(int i = 0; i < NUM_GHOSTS; i++)
        text_create(ghost_debug_text+i, &s.textures.font, "bruh");

    /* score counter */
    text_create(&score_text, &s.textures.font, " SCORE\n000000");
    score_text.pos = (vec2i){
        11,
        TILEMAP_HEIGHT-1,
    };
    text_create(&lives_text, &s.textures.font, "LIVES\n    3");
    lives_text.pos = (vec2i){
        TILEMAP_WIDTH-5,
        TILEMAP_HEIGHT-1,
    };
    text_create(&game_over_text, &s.textures.font, "GAME  OVER");
    game_over_text.pos = (vec2i){
        TILEMAP_WIDTH/2 - 5,
        TILEMAP_HEIGHT/2 - 2,
    };
    game_over_text.color.vec = (vec4){ 1.f, 0.f, 0.f, 1.f, };
    text_update(&game_over_text, true);


    /* actors */

    /* create pacman */
    {
        actor_t *pacman = &s.game.pacman;
        actor_create(pacman, &s.textures.pacman_tex);
        pacman->tile_size = 2;
    }

    /* create ghosts */
    for(int i = 0; i < NUM_GHOSTS; i++) {
        actor_t *actor = &s.game.ghosts[i].actor;
        actor_create(actor, s.textures.ghost_tex+i);
        actor->tile_size = 2;
    }

    /* create tilemap */
    glGenBuffers(2, &tilemap_vbo);

    trigger_start(&s.game.round_reset);
    trigger_start(&s.game.round_started);
}

void game_key_callback(int key, int scancode, int action, int mode) {
    if(action == GLFW_PRESS)
        switch(key) {
        case GLFW_KEY_TAB:
            s.debug.show_debug = !s.debug.show_debug;
            break;

        case 'P':
            s.debug.dump_pos = !s.debug.dump_pos;
            break;

        case 'L':
            s.debug.show_ghost_debug = !s.debug.show_ghost_debug;
            break;

        case 'G':
            s.debug.godmode = !s.debug.godmode;
            break;

        case 'R':
            trigger_start(&s.game.round_started);
            break;

        /* '-' on Nordic keyboards */
        case '/':
            s.game.lives += (mode & GLFW_MOD_SHIFT)
                ? 1 : -1;
            break;
        }
}

void game_fixed_update(double dt) {
    /* skip ticks when frozen */
    if(s.game.freeze_ticks) {
        --s.game.freeze_ticks;
        /* FIXME: jank freezing system */
        return;
    }

    if(trigger_now(s.game.game_over)) {
        show_game_over = true;

        uint64_t delay = FREEZE_GAME_OVER;
        s.game.freeze_ticks += delay;
        trigger_start_delay(&s.game.round_reset, delay+1);
        trigger_start_delay(&s.game.round_started, delay+1);
        return;
    }

    /* reset actors and tilemap */
    if(trigger_now(s.game.round_reset)) {
        show_game_over = false;
        tilemap_init();
        s.game.score = 0;
        s.game.lives = 3;
    }
    /* start new round */
    if(trigger_now(s.game.round_started)) {
        /* pacman */
        actor_t *pacman = &s.game.pacman;
        pacman->pixel_pos = (vec2i){ 14*TILE_WIDTH, 10*TILE_HEIGHT+TILE_HEIGHT/2, };
        pacman->dir = DIR_LEFT;
        /* TODO: for the love of God remove this */
        pacman->point_dir = DIR_LEFT;

        /* ghosts */
        for(int i = 0; i < NUM_GHOSTS; i++) {
            ghost_t *ghost = s.game.ghosts+i;
            ghost->type = i;
            ghost->state = GHOST_STATE_HOUSE;
            ghost->target = (vec2i){ 0, 0, };
            ghost->next_dir = DIR_UP;
            trigger_disable(&ghost->frightened);
            trigger_disable(&ghost->eaten);

            actor_t *actor = &ghost->actor;
            actor->pixel_pos = ghost_start_pos[i];
            actor->dir = DIR_UP;
        }
        {
            ghost_t *blinky = s.game.ghosts+GHOST_TYPE_BLINKY;
            blinky->state = GHOST_STATE_SCATTER;
            blinky->next_dir = DIR_LEFT;
            blinky->actor.dir = DIR_LEFT;
        }

        trigger_start(&s.game.force_leave_house);
    }

    dir_t wanted_dir = s.game.pacman.dir;
    if(key_pressed('W', NULL))
        wanted_dir = DIR_UP;
    else if(key_pressed('A', NULL))
        wanted_dir = DIR_LEFT;
    else if(key_pressed('S', NULL))
        wanted_dir = DIR_DOWN;
    else if(key_pressed('D', NULL))
        wanted_dir = DIR_RIGHT;
    pacman_controls(wanted_dir);
    if(s.debug.dump_pos) {
        const vec2i pixel_pos = s.game.pacman.pixel_pos,
                    tile_pos  = pixel_to_tile_pos(pixel_pos);
        char buf[12];
        pixel_pos_tostr(buf, sizeof(buf), pixel_pos);
        printf("pacman pos:\n  pixel: %d,%d\n  tile: %s\n  rev_tile: %d,%d\n",
               pixel_pos.x, pixel_pos.y, buf,
               tile_pos.x, TILEMAP_HEIGHT-1-tile_pos.y);
    }

    for(int i = 0; i < NUM_GHOSTS; i++) {
        ghost_t *ghost = s.game.ghosts+i;
        actor_t *a = &ghost->actor;
        ghost_update_state(ghost);
        ghost_update_sprite(ghost);
        ghost_update_target(ghost);

        const int speed = ghost_current_speed(ghost); // pixels/tick (ppt)
        for(int i = 0; i < speed; i++) {
            /* update dir in loop to not faze through walls */
            bool force = ghost_update_dir(ghost);
            if(force || can_move(a->pixel_pos, a->dir))
                a->pixel_pos = move(a->pixel_pos, a->dir);
        }
    }

    /* FIXME: jank blink animation */
    if(!(s.tick % TICK_PILL_BLINK)) {
        vec2 uv_buf[4];
        memcpy(uv_buf, s.textures.tiles[0x01].uv, sizeof uv_buf);
        memcpy(s.textures.tiles[0x01].uv, s.textures.tiles[0x25].uv, sizeof uv_buf);
        memcpy(s.textures.tiles[0x25].uv, uv_buf, sizeof uv_buf);
    }
}

void game_render_update(double dt) {
    /* update actors */
    actor_update(&s.game.pacman, true);
    for(int i = 0; i < NUM_GHOSTS; i++) {
        ghost_t *ghost = s.game.ghosts+i;
        actor_t *a = &ghost->actor;
        actor_update(a, true);

        if(s.debug.show_ghost_debug) {
            text_sprite_t *text = ghost_debug_text+i;
            char buf[12];
            pixel_pos_tostr(buf, sizeof(buf), a->pixel_pos);
            snprintf(text->str, sizeof(text->str), "%s\n%s\nn:%s\na:%s\nt:%d,%d\np:%s",
                     ghost_type_tostr(ghost->type), ghost_state_tostr(ghost->state),
                     dir_tostr(ghost->next_dir), dir_tostr(a->dir),
                     ghost->target.x, ghost->target.y, buf);

            text->scale = 1.f;
            text->pos = a->sprite.pos;
            /* TODO: switch sprites/text to pixel coords */
            text_update(text, false);
        }
    }

    /* TODO: make an actual 8x8 font */
    /* update text */
    if(s.debug.show_debug) {
        snprintf(debug_text.str, sizeof(debug_text.str),
                 "FPS:%4d\ndumppos:%s\ndumpghost:%s\ngod:%s",
                 (int)(1/s.gfx.frametime), bool_tostr(s.debug.dump_pos),
                 bool_tostr(s.debug.show_ghost_debug), bool_tostr(s.debug.godmode));
        debug_text.scale = 2.f;
        debug_text.pos = (vec2i){
            10,
            s.gfx.height-10 - (*debug_text.font)->h * debug_text.scale,
        };
        text_update(&debug_text, false);
    }
    snprintf(score_text.str, sizeof(score_text.str), " SCORE\n%06d", s.game.score);
    snprintf(lives_text.str, sizeof(lives_text.str), "LIVES\n  %3d", s.game.lives);
    text_update(&score_text, true);
    text_update(&lives_text, true);

    /* render tilemap */
    tilemap_render();

    /* render sprites */
    sprite_render_begin();
    sprite_render(&s.game.pacman.sprite);
    for(int i = 0; i < NUM_GHOSTS; i++)
        sprite_render(&s.game.ghosts[i].actor.sprite);
    sprite_render_end();

    /* render text */
    text_render_begin();
    if(s.debug.show_debug) text_render(&debug_text);
    if(s.debug.show_ghost_debug)
        for(int i = 0; i < NUM_GHOSTS; i++)
            text_render(ghost_debug_text+i);
    text_render(&score_text);
    text_render(&lives_text);
    if(show_game_over) text_render(&game_over_text);
    text_render_end();
}

void game_stop() {
    text_destroy(&debug_text);
    text_destroy(&score_text);
    for(int i = 0; i < NUM_GHOSTS; i++)
        text_destroy(ghost_debug_text+i);
    texture_destroy_sheet(&s.textures.tiles);
}
