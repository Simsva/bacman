#ifndef GAME_LOGIC_H_
#define GAME_LOGIC_H_

#include <GL/glew.h>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

#include <stdbool.h>

#include "game_types.h"

void actor_create(actor_t *out, texture_t *texture);
void actor_update(actor_t *actor, bool center);

#endif // GAME_LOGIC_H_
