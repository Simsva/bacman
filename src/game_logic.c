#include "game_logic.h"

#include <stdio.h>

#include "shaders.h"

void actor_create(actor_t *out, texture_t *texture) {
    sprite_create(&out->sprite, texture);

    out->pixel_pos = (vec2i){ 0, 0, };
    out->tile_size = 1;
    out->dir = DIR_UP;
}

void actor_update(actor_t *actor, bool center) {
    actor->sprite.pos.x = (s.gfx.width  * actor->pixel_pos.x)/TILEMAP_PIXEL_W;
    actor->sprite.pos.y = (s.gfx.height * actor->pixel_pos.y)/TILEMAP_PIXEL_H;

    actor->sprite.scale = s.gfx.height * actor->tile_size*TILEMAP_NORM_H
        / actor->sprite.tex->h;
    actor->sprite.rot = actor->point_dir;

    if(center) {
        actor->sprite.pos.x -= (actor->sprite.tex->w * actor->sprite.scale)/2;
        actor->sprite.pos.y -= (actor->sprite.tex->h * actor->sprite.scale)/2;
    }

    sprite_update(&actor->sprite);
}
