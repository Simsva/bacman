#ifndef GAME_TYPES_H_
#define GAME_TYPES_H_

#include <GLFW/glfw3.h>

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>

/******** macros ********/

/* ticks */
#ifdef DBG_HALFSPEED
#define TPS (30) /* ticks per second */
#else
#define TPS (60) /* ticks per second */
#endif
#define TARGET_TPS (60) /* target TPS, used for timings */
#define TICK_TIME (1.0/TPS)
#define TICK_DISABLED (0) /* ticks begin at 1 */

/* tilemap */
#define TILE_WIDTH (8)
#define TILE_HEIGHT (8)
#define TILEMAP_WIDTH (28)
#define TILEMAP_HEIGHT (31+6)
#define TILEMAP_PIXEL_W (TILE_WIDTH*TILEMAP_WIDTH)
#define TILEMAP_PIXEL_H (TILE_HEIGHT*TILEMAP_HEIGHT)
#define TILEMAP_NORM_W (1.0/TILEMAP_WIDTH)
#define TILEMAP_NORM_H (1.0/TILEMAP_HEIGHT)
#define TILEMAP_DOOR_X (14*TILE_WIDTH)
#define TILEMAP_DOOR_Y (22*TILE_HEIGHT+TILE_HEIGHT/2)

/* useful macros */
#define MAX(a, b) (((a) > (b)) ? (a) : (b))
#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#define CLAMP(a, min, max) ((min) < (max) \
    ? (((a) < (min)) ? (min) : (((a) > (max)) ? (max) : (a))) \
    : (((a) < (max)) ? (max) : (((a) > (min)) ? (min) : (a))) \
    )


/******** types ********/

/* generic */

/* 2d int vector */
typedef struct {
    int32_t x, y;
} vec2i;

/* 2d vector */
typedef struct {
    float x, y;
} vec2;

/* 4d vector */
typedef struct {
    float x, y, z, w;
} vec4;

/* event trigger */
typedef struct {
    uint64_t tick;
} trigger_t;


/* enums */

/* various timings */
enum {
    TICK_FRIGHT = 6*TARGET_TPS,
    TICK_PILL_BLINK = 8,
    FREEZE_GHOST_EATEN = TARGET_TPS,
    FREEZE_PACMAN_EATEN = (5*TARGET_TPS)/2,
    FREEZE_GAME_OVER = 3*TARGET_TPS,
};

/* directions */
typedef enum {
    DIR_UP = 0, /* 00 */
    DIR_LEFT,   /* 01 */
    DIR_DOWN,   /* 10 */
    DIR_RIGHT,  /* 11 */
    NUM_DIRS,
} dir_t;

/* ghost types */
typedef enum {
    GHOST_TYPE_BLINKY = 0,
    GHOST_TYPE_PINKY,
    GHOST_TYPE_INKY,
    GHOST_TYPE_CLYDE,
    NUM_GHOSTS,
} ghost_type_t;

/* ghost AI states */
typedef enum {
    GHOST_STATE_NONE,
    GHOST_STATE_CHASE,      /* currently chasing Pac-Man */
    GHOST_STATE_SCATTER,    /* currently heading to the corner scatter targets */
    GHOST_STATE_FRIGHTENED, /* frightened after Pacman has eaten an energizer pill */
    GHOST_STATE_EYES,       /* eaten by Pac-Man and heading back to the ghost house */
    GHOST_STATE_HOUSE,      /* currently inside the ghost house */
    GHOST_STATE_LEAVEHOUSE, /* currently leaving the ghost house */
    GHOST_STATE_ENTERHOUSE, /* currently entering the ghost house */
    NUM_GHOST_STATES,
} ghost_state_t;


/* shaders/textures */

/* shader program */
typedef struct {
    GLuint vert, frag, program;
} shader_t;

/*
** contains raw image data to use as an atlas
**/
typedef struct {
    uint32_t w, h;
    GLubyte *bytes;
} texture_atlas_t;

/* coordinates to a texture from an atlas */
typedef struct {
    texture_atlas_t *atlas;
    uint32_t w, h;
    union {
        vec2 vec;
        float arr[2];
    } uv[4];
} texture_t;

/* ASCII fonts */
typedef texture_t font_t[128];

/* generic texture vertex */
typedef struct {
    float x, y,
          u, v;
} texture_vertex_t;


/* "game objects" */

/*
** information needed to draw a texture (position + scale)
** including vertices + VBO
**/
typedef struct {
    texture_t *tex;
    /* int32_t x, y; */
    vec2i pos;
    float scale;

    /* TODO: remove this */
    /* DIR_UP is default */
    dir_t rot;

    /*
    ** Vertex order:
    ** 2--3
    ** |  |
    ** 0--1
    */
    texture_vertex_t _vertices[4];
    GLuint _vbo;
} sprite_t;

/* text to render */
typedef struct {
    font_t *font;
    char str[128];
    /* int32_t x, y; */
    vec2i pos;
    float scale;
    union {
        vec4 vec;
        float arr[4];
    } color;

    size_t _len;
    texture_vertex_t *_vertices;
    GLushort *_indices;
    GLuint _vbo, _ibo;
} text_sprite_t;

/* actor using tilemap coordinates */
typedef struct {
    sprite_t sprite;
    vec2i pixel_pos;
    int32_t tile_size;
    /* NOTE: this is stupid */
    dir_t point_dir,
          dir;
} actor_t;

/* ghost actor */
typedef struct {
    actor_t actor;
    ghost_type_t type;
    ghost_state_t state;
    vec2i target;
    /* ghosts think one tile in the future in chase/scatter mode */
    dir_t next_dir;

    trigger_t frightened, eaten;
} ghost_t;


/* current game state */
extern struct game_state_t {
    /* graphics related state variables */
    struct {
        GLFWwindow *win, *dummy_win;
        int width, height;
        float ratio;

        uint8_t render_loop_on;
        double fps_freq, frametime;

        uint8_t tilemap[TILEMAP_HEIGHT-6][TILEMAP_WIDTH];
    } gfx;

    struct {
        shader_t sprite_shader, font_shader;
    } shaders;

    struct {
        font_t font;
        texture_t *tiles,
            pacman_tex,
            ghost_tex[NUM_GHOSTS], ghost_eyes, ghost_frightened;
    } textures;

    struct {
        uint8_t key_press[GLFW_KEY_LAST+1];
    } input;

    struct {
        actor_t pacman;
        ghost_t ghosts[NUM_GHOSTS];
        uint64_t freeze_ticks;
        uint32_t score;
        uint8_t ghosts_eaten, lives;

        trigger_t force_leave_house, round_reset, round_started, game_over;
    } game;

    struct {
        bool show_debug, show_ghost_debug, dump_pos, godmode;
    } debug;

    uint64_t tick;
    int exit_code;
} s;


/******** functions ********/

/* vec2i math */

static vec2i vec2i_add(vec2i a, vec2i b) {
    return (vec2i){ a.x+b.x, a.y+b.y };
}

static vec2i vec2i_sub(vec2i a, vec2i b) {
    return (vec2i){ a.x-b.x, a.y-b.y };
}

static vec2i vec2i_scale(vec2i a, int32_t s) {
    return (vec2i){ a.x*s, a.y*s };
}

static vec2i dir_to_vec2i(dir_t dir) {
    const vec2i vecs[NUM_DIRS] = {
        { 0, 1, }, { -1, 0, }, { 0, -1, }, { 1, 0, },
    };
    return vecs[dir];
}

static int32_t vec2i_sqrdist(vec2i a, vec2i b) {
    const vec2i d = vec2i_sub(a, b);
    return d.x*d.x + d.y*d.y;
}

static bool vec2i_near(vec2i a, vec2i b, uint32_t tol) {
    return (abs(a.x - b.x) <= tol) && (abs(a.y - b.y) <= tol);
}

static bool vec2i_equal(vec2i a, vec2i b) {
    return (a.x == b.x) && (a.y == b.y);
}

static vec2i pixel_to_tile_pos(vec2i pixel_pos) {
    return (vec2i){ pixel_pos.x / TILE_WIDTH, pixel_pos.y / TILE_HEIGHT };
}

static vec2i dist_to_tile_mid(vec2i pos) {
    return (vec2i){
        (TILE_WIDTH/2) - pos.x % TILE_WIDTH,
        -(TILE_HEIGHT/2) + pos.y % TILE_HEIGHT,
    };
}


/* triggers */

/* check if trigger is active for current tick */
static inline bool trigger_now(trigger_t t) {
    return s.tick == t.tick;
}

/* activate trigger next tick */
static inline void trigger_start(trigger_t *t) {
    t->tick = s.tick + 1;
}

/* activate trigger after `delay` ticks */
static inline void trigger_start_delay(trigger_t *t, uint64_t delay) {
    t->tick = s.tick + delay;
}

/* disable trigger */
static inline void trigger_disable(trigger_t *t) {
    t->tick = TICK_DISABLED;
}

/* time since trigger */
static inline uint64_t trigger_since(trigger_t t) {
    return s.tick - t.tick;
}

/* if t activated before tick */
static inline bool trigger_before(trigger_t t, uint64_t tick) {
    uint64_t s = trigger_since(t);
    return (t.tick == TICK_DISABLED) ? false : (s < tick);
}


/* exits the game safely (calling game_stop) */
static void exit_safe(int status) {
    s.exit_code = status;
    glfwSetWindowShouldClose(s.gfx.win, GL_TRUE);
}

/* check if key is pressed */
static bool key_pressed(int key, uint8_t *mod) {
    if(key > GLFW_KEY_LAST || key < 0) return 0;
    uint8_t *k = s.input.key_press+key;
    if(mod) *mod = *k & ~0x80;
    return *k;
    /* uint8_t out = *k; */
    /* *k = 0; */
    /* return out; */
}

/* helper function to rotate sprite vertices
** DIR_UP is default
** NOTE: deprecated */
static int rotate_vertex(int i, dir_t dir) {
    const int rots[NUM_DIRS][4] = {
        {0, 1, 2, 3},
        {1, 3, 0, 2},
        {3, 2, 1, 0},
        {2, 0, 3, 1},
    };

    return rots[dir][i];
}

static vec2i tilemap_clamp(vec2i pos) {
    return (vec2i){
        CLAMP(pos.x, 0, TILEMAP_WIDTH-1),
        CLAMP(pos.y, 3, TILEMAP_HEIGHT-4),
    };
}

/* get opposite direction */
static dir_t opposite_dir(dir_t dir) {
    const dir_t dirs[NUM_DIRS] = { DIR_DOWN, DIR_RIGHT, DIR_UP, DIR_LEFT, };
    return dirs[dir];
}


/* tostr functions for debugging */
/* TODO: macro to generate these from enums? */

static const char *dir_tostr(dir_t dir) {
    const char *str[NUM_DIRS] = {
        "UP",
        "LEFT",
        "DOWN",
        "RIGHT",
    };
    return str[dir];
}

static const char *ghost_state_tostr(ghost_state_t state) {
    const char *states[NUM_GHOST_STATES] = {
        "NONE",
        "CHASE",
        "SCATTER",
        "FRIGHTENED",
        "EYES",
        "HOUSE",
        "LEAVEHOUSE",
        "ENTERHOUSE",
    };
    return states[state];
}

static const char *ghost_type_tostr(ghost_type_t type) {
    const char *types[NUM_GHOSTS] = {
        "BLINKY",
        "PINKY",
        "INKY",
        "CLYDE",
    };
    return types[type];
}

static const char *bool_tostr(bool b) {
    return b ? "true" : "false";
}

static void pixel_pos_tostr(char *str, size_t n, vec2i pixel_pos) {
    const vec2i tile_pos = pixel_to_tile_pos(pixel_pos),
                mid_d = dist_to_tile_mid(pixel_pos);
    snprintf(str, n, "%d%+d,%d%+d",
             tile_pos.x, mid_d.x, tile_pos.y, mid_d.y);
}

#endif // GAME_TYPES_H_
