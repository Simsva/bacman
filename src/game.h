#ifndef GAME_H_
#define GAME_H_

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "game_types.h"

void game_init();
void game_key_callback(int key, int scancode, int action, int mode);
void game_fixed_update(double dt);
void game_render_update(double dt);
void game_stop();

#endif // GAME_H_
