#include "config.h"
#include "debug.h"

#ifdef _WIN32
# error Windows is not supported as of yet
#endif

#include <GL/glew.h>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <pthread.h>

#include "game_types.h"
#include "game.h"

#define WINDOW_WIDTH  (1920)
#define WINDOW_HEIGHT (1080)
#define WINDOW_NAME   "Pac-Man"

struct game_state_t s;

void glfw_error_callback(int err, const char *desc) {
    fprintf(stderr, "[GLFW Error] %s\n", desc);
}

void glfw_key_callback(GLFWwindow *w, int key, int scancode, int action, int mode) {
    if(action == GLFW_PRESS || action == GLFW_REPEAT)
        s.input.key_press[key] = mode ? mode : 0x80;
    else if(action == GLFW_RELEASE)
        s.input.key_press[key] = 0x00;

    game_key_callback(key, scancode, action, mode);
}

void *render_loop(void *arg) {
    glfwMakeContextCurrent(s.gfx.win);

    uint32_t fps_frames = 0;
    double dt, dt_prev, fps_prev;
    dt_prev = fps_prev = glfwGetTime();
    while(s.gfx.render_loop_on) {
        dt = glfwGetTime() - dt_prev;
        dt_prev = glfwGetTime();

        /* FPS counter */
        ++fps_frames;
        if(glfwGetTime() - fps_prev > s.gfx.fps_freq) {
            s.gfx.frametime = s.gfx.fps_freq/fps_frames;
            /* printf("----------------------\nframetime: %.6fms\nfps: %.1f\n----------------------\n", */
            /*        s.gfx.frametime*1000, 1/s.gfx.frametime); */
            fps_frames = 0;
            fps_prev += s.gfx.fps_freq;
        }

        int w, h;
        glfwGetFramebufferSize(s.gfx.win, &w, &h);
        if(w > s.gfx.ratio * h) {
            s.gfx.width = h * s.gfx.ratio;
            s.gfx.height = h;
        } else if(w < s.gfx.ratio * h) {
            s.gfx.width = w;
            s.gfx.height = w / s.gfx.ratio;
        }

        glViewport(abs(s.gfx.width-w)/2, abs(s.gfx.height-h)/2,
                   s.gfx.width, s.gfx.height);

        glClear(GL_COLOR_BUFFER_BIT);

        /* game_render_update event */
        game_render_update(dt);

        glfwSwapBuffers(s.gfx.win);
    }

    return NULL;
}

int main(int argc, char *argv[]) {
    pthread_t render_thread;

    srand(time(NULL));

    /* init state */
    s.tick = 0;
    s.exit_code = 0;
    s.gfx.render_loop_on = 1;
    s.gfx.fps_freq = 0.5;

    /* init GLFW */
    glfwSetErrorCallback(&glfw_error_callback);
    if(!glfwInit()) {
        s.exit_code = -1;
        goto ret;
    }

    /* init real and dummy context */
    s.gfx.width = WINDOW_WIDTH;
    s.gfx.height = WINDOW_HEIGHT;

    glfwWindowHint(GLFW_VISIBLE, GL_TRUE);
    s.gfx.win = glfwCreateWindow(s.gfx.width, s.gfx.height, WINDOW_NAME, NULL, NULL);
    glfwWindowHint(GLFW_VISIBLE, GL_FALSE);
    s.gfx.dummy_win = glfwCreateWindow(1, 1, WINDOW_NAME " Dummy", NULL, NULL);
    if(!s.gfx.win || !s.gfx.dummy_win) {
        glfwTerminate();
        s.exit_code = -1;
        goto ret;
    }
    glfwMakeContextCurrent(s.gfx.win);
    glfwSetKeyCallback(s.gfx.win, glfw_key_callback);
    glfwSwapInterval(0);

    /* init GL in real context */
    glewExperimental = GL_TRUE;
    GLenum err = glewInit();
    if(err != GLEW_OK) {
        fprintf(stderr, "[GLEW ERR] glewInit failed: %s\n", glewGetErrorString(err));
        glfwTerminate();
        s.exit_code = -1;
        goto ret;
    }

    /* game_init event */
    game_init();

    /* switch context and start render thread */
    glfwMakeContextCurrent(s.gfx.dummy_win);
    pthread_create(&render_thread, NULL, render_loop, NULL);

    double dt_prev, dt;
    dt_prev = glfwGetTime();
    while(!glfwWindowShouldClose(s.gfx.win)) {
        if(glfwGetTime() - dt_prev >= TICK_TIME) {
            dt = glfwGetTime() - dt_prev;
            dt_prev += TICK_TIME;
            ++s.tick;

            glfwPollEvents();

            /* game_fixed_update event */
            game_fixed_update(dt);
        }
    }

    s.gfx.render_loop_on = 0;
    pthread_join(render_thread, NULL);

    /* game_stop event */
    glfwMakeContextCurrent(s.gfx.win);
    game_stop();

    glfwDestroyWindow(s.gfx.win);
    glfwDestroyWindow(s.gfx.dummy_win);
    glfwTerminate();

ret:
    return s.exit_code;
}
