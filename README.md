# Bacman

A bad clone of Pac-Man written in C and OpenGL

## External Dependencies

- GNU Autotools + GNU Make
- GLFW3
- GLEW
- OpenGL 4.60 support
- libpng

## Building

This project uses the GNU Autotools build system, so the easiest way to build and install it would be to run:

```sh
autoreconf -i # Only run this on the first build
./configure
make && make install
```

Of course, build settings can be changed using the `./configure` script.

## Legal Stuff

Some source code from [pacman.c](https://github.com/floooh/pacman.c) was used. A copy of that program's license can be found in `LICENSE.pacman_c`.
