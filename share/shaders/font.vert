#version 460

layout(location = 0) in vec2 pos;
layout(location = 1) in vec2 uv_in;
layout(location = 2) uniform vec4 color_in = vec4(1.0);
out vec2 uv;
out vec4 color;

void main()
{
    gl_Position = vec4((pos - 0.5) * 2.0, 0.0, 1.0);
    uv = uv_in;
    color = color_in;
}
