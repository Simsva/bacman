#version 460

layout(binding = 1) uniform sampler2D tex;
in vec2 uv;
in vec4 color;
out vec4 color_out;

void main() {
    color_out = texture(tex, uv) * color;
    // color_out = vec4(1.0);
}
